package creant_llibreries_utilities;

import java.util.function.Predicate;

public interface UtilLambda {
	static void run(Runnable function) {
		function.run();
	}
	
	public enum TipusProducte {
        ALIMENT("Aliment") {
            @Override
            public void imprimirInformacio() {
                System.out.println("Aquest producte és un aliment.");
            }
        },
        ELECTRONICA("Electrònica") {
            @Override
            public void imprimirInformacio() {
                System.out.println("Aquest producte és d'electrònica.");
            }
        },
        ROBA("Roba") {
            @Override
            public void imprimirInformacio() {
                System.out.println("Aquest producte és de roba.");
            }
        },
        JOGUINA("Joguina") {
            @Override
            public void imprimirInformacio() {
                System.out.println("Aquest producte és una joguina.");
            }
        };
        
        private final String descripcio;
        
        private TipusProducte(String descripcio) {
            this.descripcio = descripcio;
        }
        
        public String getDescripcio() {
            return descripcio;
        }
        
        public abstract void imprimirInformacio();
    }
	
	public enum ExtensioFitxer {
		TXT("txt", "Fitxer de text"),
	    PDF("pdf", "Fitxer PDF"),
	    DOC("doc", "Fitxer de document de Microsoft Word");

	    private final String extensio;
	    private final String descripcio;

	    ExtensioFitxer(String extensio, String descripcio) {
	        this.extensio = extensio;
	        this.descripcio = descripcio;
	    }

	    public String obtenirDescripcio() {
	        return descripcio;
	    }
	}
	
	public enum DiaSetmana {
	    DILLUNS("Dilluns"),
	    DIMARTS("Dimarts"),
	    DIMECRES("Dimecres"),
	    DIJOUS("Dijous"),
	    DIVENDRES("Divendres"),
	    DISSABTE("Dissabte"),
	    DIUMENGE("Diumenge");

	    private final String nom;
	    private Predicate<DiaSetmana> esDiumenge;

	    DiaSetmana(String nom) {
	        this.nom = nom;
	        this.esDiumenge = d -> false;
	    }

	    private void init(Predicate<DiaSetmana> esDiumenge) {
	        this.esDiumenge = esDiumenge;
	    }

	    DiaSetmana(String nom, Predicate<DiaSetmana> esDiumenge) {
	        this.nom = nom;
	        init(esDiumenge);
	    }

	    public boolean esDiumenge() {
	        return esDiumenge.test(this);
	    }

	    @Override
	    public String toString() {
	        return nom;
	    }

	    static {
	        DIUMENGE.init(d -> d == DIUMENGE);
	    }
	}
}