package CaixaNegraBlanca;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Ex1CNTest {

	@Test
	void test() {
		assertEquals(37.4,Ex1CN.conversorCelsiusFahrenheit(3.0));
		assertEquals(112.28,Ex1CN.conversorCelsiusFahrenheit(44.6));
		assertEquals(44.6,Ex1CN.conversorCelsiusFahrenheit(7.0));
		assertEquals(48.2,Ex1CN.conversorCelsiusFahrenheit(9.0));
		assertEquals(57.74,Ex1CN.conversorCelsiusFahrenheit(14.3));
		assertEquals(85.28,Ex1CN.conversorCelsiusFahrenheit(29.6));
		assertEquals(1938.2,Ex1CN.conversorCelsiusFahrenheit(1059));
		assertEquals(33.8,Ex1CN.conversorCelsiusFahrenheit(1.0));
		assertEquals(92.84,Ex1CN.conversorCelsiusFahrenheit(33.8));
		assertEquals(71.6,Ex1CN.conversorCelsiusFahrenheit(22));
	}
}