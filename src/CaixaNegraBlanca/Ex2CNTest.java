package CaixaNegraBlanca;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Ex2CNTest {

	@Test
	void test() {
		assertEquals(20.0,Ex2CN.adivinanumeros(1,55,34,2,89));
		assertEquals(40.0,Ex2CN.adivinanumeros(4,8,34,43,63));
		assertEquals(60.0,Ex2CN.adivinanumeros(7,3,25,33,63));
		assertEquals(80.0,Ex2CN.adivinanumeros(88,75,63,52,1));
		assertEquals(40.0,Ex2CN.adivinanumeros(1,2,3,4,25));
		assertEquals(100.0,Ex2CN.adivinanumeros(8,25,33,42,52));
		assertEquals(60.0,Ex2CN.adivinanumeros(88,75,63,4,5));
		assertEquals(80.0,Ex2CN.adivinanumeros(50,52,63,25,16));
		assertEquals(80.0,Ex2CN.adivinanumeros(33,16,51,63,88));
		assertEquals(20.0,Ex2CN.adivinanumeros(4,25,26,27,28));
	}
}